title:	Störungsmeldung
body:	Das Netzwerk "Verkehrsauskunft Österreich" wurde auf Wunsch der VAO GmbH aus Öffi entfernt.

Um das Problem zu umgehen, kannst du Öffi auf ein anderes Verkehrsnetz, das deine Region abdeckt, umschalten.
button-neutral:		dismiss
